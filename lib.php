<?php 

function btcid_query($method = null, $req = array()) {

    // API settings 
    $key = ''; // PUT YOUR API KEY HERE
    $secret = ''; // PUT YOUR API SECRET HERE

    $req['method'] = $method;
    $req['nonce'] = time();

    // generate the POST data string
    $post_data = http_build_query($req, '', '&');
    $sign = hash_hmac('sha512', $post_data, $secret);
    
    // generate the extra headers
    $headers = array(
            'Sign: '.$sign,
            'Key: '.$key,
    );

    // our curl handle (initialize if required)
    static $ch = null;
    if (is_null($ch)) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible;
        BITCOINCOID PHP client; '.php_uname('s').'; PHP/'.phpversion().')');
    }

    curl_setopt($ch, CURLOPT_URL, 'https://vip.bitcoin.co.id/tapi/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
    // run the query
    $res = curl_exec($ch);
    if ($res === false) throw new Exception('Could not get reply:'.curl_error($ch));
        $dec = json_decode($res, true);

    if (!$dec) throw new Exception('Invalid data received, please make sure connection is working and requested API exists: '.$res);
    
    curl_close($ch);
    $ch = null;

    // if($dec['success'] == 0) {
    //     pr($req);
    //     pr($dec);
    //     echo "<script type='text/javascript'>window.location.reload();</script>";

    //     die;
    // }

    // sleep(2000);

    return $dec;    
}

function btcid_get($url) {
	$ssl = false;
	if (preg_match('/^https/i', $url))
	{
		$ssl = true;
	}
	$ch = curl_init();
	if ($ssl)
	{
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
	@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	$res = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);
	unset($ch);
	return array(
		'response' => trim($res),
		'info' => $info
	);
}

function number_display($number) {
    if($number > 0) {
        echo "<STRONG class='green'>";        
    } elseif($number < 0) {
        echo "<STRONG class='red'>";        
    }

    echo "IDR ";
    echo number_format($number);
    echo "</STRONG>";
}

function pr($array) {
	echo "<PRE>";
	print_r($array);
	echo "</PRE>";
}

function array_clean_merge($array1, $array2) {

    $new_array = array();

    // do clean up
    foreach($array1 as $key => $value) {
        if($value > 0) {
            $new_array[$key] = $value;        
        }
    }

    foreach($array2 as $key => $value) {
        if($value > 0) {
            $new_array[$key] = $value;        
        }
    }

    asort($new_array); // sort by key

    return $new_array;
}

?>