<?php 

$date = new DateTime();
$date->setTimezone(new DateTimeZone('Asia/Jakarta')); // set time zone
$datetime = $date->format('Y-m-d H:i:s') . "\n";

$data = btcid_query('getInfo');
$balance = array_clean_merge($data['return']['balance'], $data['return']['balance_hold']);
$saldo = $balance['idr'];

// pr($saldo);

unset($balance['idr']); // remove IDR 

$user = array();
$user['name'] = $data['return']['name'];
$user['profile_picture'] = $data['return']['profile_picture'];

$buy_data = array();
foreach($balance as $key => $value) {
    if($value > 0 && $key != "idr") {                    

        do {
            $trade_data = btcid_query('tradeHistory', array("pair" => $key . "_idr")); // get buy position
            sleep(1);                  
        } while ($trade_data['success'] == 0);            

        //get current price
        $response = btcid_get('https://vip.bitcoin.co.id/api/' . $key . '_idr/ticker');
        $current_price = json_decode($response['response'], true); // format to array        
        $current_price = $current_price['ticker']['sell'];                        

        // sleep(1); // provides delay to avoid API request limit of 180 request per minute
        
        $buy_data[$key]['buy_at'] = $trade_data['return']['trades'][0]['price'];        
        $buy_data[$key]['current_price'] = $current_price;
        $buy_data[$key]['profit'] = $current_price - $buy_data[$key]['buy_at'];
        $buy_data[$key]['profit_percentage'] = round(($buy_data[$key]['profit'] / $buy_data[$key]['buy_at']) * 100, 2);
        $buy_data[$key]['total'] = $buy_data[$key]['profit'] * $value;        
    }        
}    

// pr($buy_data);

?>   