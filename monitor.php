<!DOCTYPE html>
<html>
<head>
    <?php 
        require_once('settings-monitor.php'); 
        require_once('lib.php'); 
        require_once('calc-monitor.php'); 
    ?>
    <title>Bitcoin.co.id price checker</title>

    <!-- Main CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <script type="text/javascript">    
        // setInterval(function() { console.log('one second has passed') }, 1000); 
        setInterval(function() { window.location.reload(); }, 60000); 
    </script>

</head>
<body> 

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <h1>Bitcoin.co.id price checker</h1>

                <p>Account name : <strong><?php echo $user['name'] ?></strong> (IDR <?php echo number_format($saldo) ?> remaining)</p>

                <?php pr($data); ?>

                <!-- <table class="table table-stripped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Balance</th>
                            <th>Balance (IDR)</th>
                            <th>Buy at</th>
                            <th>Current price</th>
                            <th>Difference</th>
                            <th>Profit %</th>
                            <th>Total Profit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total_balance = 0; $total_profit = 0; ?>
                        <?php foreach($balance as $key => $value): ?>
                        <?php if($value > 0): ?>
                        <tr>                            
                            <td><strong><?php echo strtoupper($key) ?></strong></td>
                            <td><?php echo $value ?></td>   
                            <td>IDR <?php echo number_format($value * $buy_data[$key]['current_price']) ?></td>
                            <td><?php echo number_format($buy_data[$key]['buy_at']) ?></td>
                            <td><?php echo number_format($buy_data[$key]['current_price']) ?></td>
                            <td><?php echo number_display($buy_data[$key]['profit']) ?></td>
                            <td><?php echo $buy_data[$key]['profit_percentage'] ?>%</td>
                            <td><?php echo number_display($buy_data[$key]['total']) ?></td>
                        </tr>
                        <?php endif; ?>
                        <?php
                            // calculate total
                            $total_balance += $value * $buy_data[$key]['current_price']; 
                            $total_profit += $buy_data[$key]['total'];
                        ?>
                        <?php endforeach; ?>
                        <tr class="info">
                            <td><strong>TOTAL</strong></td>
                            <td></td>
                            <td><strong>IDR <?php echo number_format($total_balance) ?></strong></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><?php echo number_display($total_profit) ?></td>
                        </tr>
                    </tbody>
                </table> -->

                <p>Generated on <strong><?php echo $datetime; ?></strong></p>
            </div>
        </div>
    </div>

</body>
</html>